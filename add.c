#include"paimon.h"

int add(struct node *list, char *entry){

	struct node *new;
	struct node *tmp;
	int i;
	char *tmps;

	tmps = malloc((strlen(entry)-1)*sizeof(char));
	i = 0;

	while(strlen(entry) > i){
	
		if((entry[i] <= 122) && (entry[i] >= 97)){
		
			tmps[i] = entry[i] - 32;
		
		}else{
		
			tmps[i] = entry[i];
		
		}

		i++;
	
	}

	if(list->next == NULL){

		new = (struct node*)malloc(sizeof(struct node));
		new->data = malloc((strlen(entry)-1)*sizeof(char));
		new->fdata = malloc((strlen(entry)-1)*sizeof(char));
		strcpy(new->data, entry);
		strcpy(new->fdata, tmps);
		list->next = new;
		new->next = NULL;

		i = 1;
	
	}else{

		if(strcmp(list->next->fdata, tmps) < 0){

			tmp = list->next;

			i = add(tmp, entry);

		}else if(strcmp(list->next->data, tmps) > 0){

			new = (struct node*)malloc(sizeof(struct node));
			new->data = malloc((strlen(entry)-1)*sizeof(char));
			new->fdata = malloc((strlen(entry)-1)*sizeof(char));
			strcpy(new->data, entry);
			strcpy(new->fdata, tmps);
			new->next = list->next;
			list->next = new;

			i = 1;
			
		}else{

			i = 2;					

		}
	
	}

	free(tmps);
	return i;

}
