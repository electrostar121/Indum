indum: main.o init.o add.o print.o delete.o search.o cleanList.o paimon.h subList.o
	gcc -g -o indum main.o init.o add.o print.o delete.o search.o cleanList.o paimon.h subList.o
main.o:	main.c
	gcc -c main.c
init.o: init.c
	gcc -c init.c
add.o: add.c
	gcc -c add.c
print.o: print.c
	gcc -c print.c
delete.o: delete.c
	gcc -c delete.c
search.o: search.c
	gcc -c search.c
cleanList.o: cleanList.c
	gcc -c cleanList.c
subList.o: subList.c
	gcc -c subList.c
clean:
	rm -f indum main.o init.o add.o print.o delete.o search.o parse.o cleanList.o subList.o
