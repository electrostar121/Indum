#include"paimon.h"

void delete(struct node *list, char *entry){

	struct node *tmp;
	
	tmp = list;
	list = list->next;

	while(list != NULL){

		if(strcmp(list->data, entry) == 0){

			tmp->next = list->next;

			free(list->data);
			free(list->fdata);
			free(list);

			list = tmp->next;
		
		}else{

			tmp = list;
			list = list->next;
		
		}
	
	}

}
