#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct node{

	char *data;
	char *fdata;
	struct node *next;

};

struct node *init();
int add(struct node *list, char *entry);
void print(struct node *list);
void delete(struct node *list, char *entry);
int search(struct node *list, char *entry);
void cleanList(struct node *list);
int subList(char *entry);
