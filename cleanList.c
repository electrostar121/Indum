#include"paimon.h"

void cleanList(struct node *list){

	struct node *next;

	while(list != NULL){

		next = list->next;

		free(list->data);
		free(list->fdata);
		free(list);

		list = next;
	
	}

}
